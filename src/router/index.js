import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/pages/Index'
import 领导指示 from '@/pages/领导指示'
import 在线填报 from '@/pages/在线填报'
import 部门登记 from '@/pages/部门登记'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: Index
    },
    {
      path: '/领导指示',
      name: '领导指示',
      component: 领导指示
    },
    {
      path: '/在线填报',
      name: '在线填报',
      component: 在线填报
    },
    {
      path: '/部门登记',
      name: '部门登记',
      component: 部门登记
    }
  ]
})
